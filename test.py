import unittest
from main import ManualTree
import pandas as pd

NOT_PLAY = 0
PLAY = 1


class DecisionTreeTest(unittest.TestCase):
    def setUp(self):
        # Load data from file txt
        self.tennis = pd.read_csv("tennis_data.csv")

    def test_manual_tree(self):
        sample = {
            "Outlook": "Rain",
            "Humidity": "High",
            "Wind": "Weak"
        }

        tree = ManualTree()
        prediction = tree.predict_one(sample)

        self.assertEqual(prediction, PLAY)

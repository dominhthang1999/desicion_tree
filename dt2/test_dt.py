# Updated by Andrew to support Python 3
import unittest
import DT
import numpy as np
import pandas as pd

class DecisionTree2Tests(unittest.TestCase):

    def load_tennis_data(self):
        df = pd.read_csv("tennis.txt")
        return df

    def load_catdog_data(self):
        df = pd.read_csv("../catdog/data/better.txt")
        animal_index = 0
        # new outcome column
        df['outcome'] = None
        for a in df.animal.unique():
            animal_index += 1
            df.loc[df['animal'] == a, 'outcome'] = animal_index
        return df

    def setUp(self):

        # load tennis data
        self.df_tennis = self.load_tennis_data()
        self.df_catdog = self.load_catdog_data()

    def test_split_data(self):
        """Test data split.
        """

        x_values = self.df_catdog[["legs","fur","tail","size","sound"]]
        y_values = self.df_catdog[["animal"]]

        self.assertEqual(5, len(x_values.columns))
        self.assertEqual(1, len(y_values.columns))


    def Xtest_best_attr(self):
        """Test correlation.
        """

        x_values = self.df_catdog[["legs","fur","tail","size","sound"]]
        y_values = self.df_catdog[["outcome"]]

        dt = DT.SimpleTree()
        best_attr = dt.bestAttr(x_values, y_values)

        self.assertEqual(best_attr, "fur")

    def test_build_tree_manual(self):
        """Test build_tree.
        """



        pass

        # x_values = self.df_catdog[["legs","fur","tail","size","sound"]]
        # y_values = self.df_catdog[["outcome"]]
        #
        # dt = DT.DecisionTree2()
        # tree = dt.build_tree(xdata=x_values, ydata=y_values)

    def test_manual_tree(self):

        sample = {
            "Day": 15,
            "Outlook": "Rain",
            'Humidity': "High",
            'Wind': "Weak"
        }

        tree = DT.ManualTree()
        prediction = tree.predict_one(sample)


        self.assertEqual(prediction, 1)

import numpy as np
import pandas as pd

class TNode:

    LEFT = None
    RIGHT = None
    MIDDLE = None
    attr = None
    splitValue = None
    decisionFn = None
    isLeaf = False
    leafValue = None

    def __init__(self):
        pass

    def set_leaf(self, leaf_value):
        self.isLeaf = True
        self.leafValue = leaf_value

    def set_left(self, node):
        self.LEFT = node

    def set_right(self, node):
        self.RIGHT = node

    def set_middle(self, node):
        self.MIDDLE = node

    def set_decision(self, decision_fn):
        self.decisionFn = decision_fn

    def nodeType(self):
        return "NODE" if not self.isLeaf else "LEAF"

    def evaluate(self, sample):

        # if this node is a leaf, just return its value
        if self.isLeaf:
            return self.leafValue

        # decision function yields TRUE or FALSE
        direction = self.decisionFn(sample)
        if direction == "LEFT":
            return self.LEFT.evaluate(sample)
        elif direction == "MIDDLE":
            return self.MIDDLE.evaluate(sample)
        else:
            return self.RIGHT.evaluate(sample)

class ManualTree:

    def __init__(self):
        ROOT = TNode()
        ROOT.set_decision(lambda x: "LEFT" if x['Outlook'] == "Sunny" else "MIDDLE" if x['Outlook'] == "Overcast" else "RIGHT")

        # left branch
        L1 = TNode()
        L1.set_decision(lambda x: "LEFT" if x['Humidity'] == "High" else "RIGHT")

        L2 = TNode()
        L2.set_leaf(0)      # class 0 = Do not play
        L1.LEFT = L2        # connect the branch

        L3 = TNode()
        L3.set_leaf(1)      # class 1 = Play
        L1.RIGHT = L3       # connect

        # middle branch
        M1 = TNode()
        M1.set_leaf(1)      # class 1 = Play

        # right branch
        R1 = TNode()
        R1.set_decision(lambda x: "LEFT" if x['Wind'] == "Weak" else "RIGHT")

        R2 = TNode()
        R2.set_leaf(1)      # play
        R1.LEFT = R2

        R3 = TNode()
        R3.set_leaf(0)      # no play
        R1.RIGHT = R3

        ROOT.LEFT = L1
        ROOT.MIDDLE = M1
        ROOT.RIGHT = R1

        self.ROOT = ROOT

    def predict_one(self, sample):
        return self.ROOT.evaluate(sample)


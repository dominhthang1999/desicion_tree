---
title: "R Decision Trees - Part 1"
author: "Andrew"
date: "10/21/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# R Decision Trees Part 1

#### We will use simple data from the video:

```{r read_data}
df = read.csv('tennis.txt')
df
```

#### Now we will make some basic functions for the decision tree.

```{r tree_structure, echo=FALSE}

# Reference: create an empty data frame 
# https://stackoverflow.com/questions/10689055/create-an-empty-data-frame
# TREE structure
ROOT = data.frame(NODE=character(),
                  TYPE=character(),
                  DECISION_FN=character(),
                  LEFT=character(0),
                  RIGHT=character(0),
                  MIDDLE=character(0),
                  CLASS_VAL=character(0))
ROOT[nrow(ROOT)+1, ] <- c("ROOT", "DECISION", "fn_decision_root", "L1", "R1", "M1", NA)
ROOT[nrow(ROOT)+1, ] <- c("L1", "DECISION", "fn_decision_L1", "L2", "L3", NA, NA)
ROOT[nrow(ROOT)+1, ] <- c("L2", "LEAF", NA, NA, NA, NA, "No")
ROOT[nrow(ROOT)+1, ] <- c("L3", "LEAF", NA, NA, NA, NA, "Yes")
ROOT[nrow(ROOT)+1, ] <- c("M1", "LEAF", NA, NA, NA, NA, "Yes")
ROOT[nrow(ROOT)+1, ] <- c("R1", "DECISION", "fn_decision_R1", "R2", "R3", NA, NA)
ROOT[nrow(ROOT)+1, ] <- c("R2", "LEAF", NA, NA, NA, NA, "Yes")
ROOT[nrow(ROOT)+1, ] <- c("R3", "LEAF", NA, NA, NA, NA, "No")

DT.TREE = ROOT

```

#### Decision Functions

```{r decision_functions}

fn_decision_root = function(sample) {
  # node is the part of the tree
  # sample is the value we are predicting
  if (sample[1,"outlook"] == "Sunny") {
    return ("LEFT")
  }
  else if (sample[1,"outlook"] == "Overcast") {
    return ("MIDDLE")
  }
  else {
    return ("RIGHT")
  }
}

fn_decision_L1 = function(sample) {
  # node is the part of the tree
  # sample is the value we are predicting
  if (sample[1,"humidity"] == "High") {
    return ("LEFT")
  }
  else {
    return ("RIGHT")
  }
}

fn_decision_R1 = function(sample) {
  # node is the part of the tree
  # sample is the value we are predicting
  if (sample[1,"wind"] == "Weak") {
    return ("LEFT")
  }
  else {
    return ("RIGHT")
  }
}


```

#### Prediction Logic

```{r tree_logic}
DT.predict = function(sample) {
  curr_node = "ROOT"
  curr_row = DT.TREE[which(DT.TREE$NODE == curr_node), ]  

  # while not leaf
  while(curr_row[1,c("TYPE")] != "LEAF") { 
    # find out which way to go
    which_way = do.call(curr_row[1, 3], c(list(new_sample)))
    next_node = curr_row[1,which_way]
    curr_node = next_node
    curr_row = DT.TREE[which(DT.TREE$NODE == curr_node), ]
  }
  return (curr_row[1,c("CLASS_VAL")])
}


```

#### Try it!

```{r try_predict}

new_sample = data.frame(day=character(),
                        outlook=character(),
                        humidity=character(),
                        wind=character())
new_sample[1,] = c('15','Rain','High','Weak')

DT.predict(new_sample)

```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

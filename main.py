import pandas as pd
import numpy as np

NOT_PLAY = 0
PLAY = 1


class ManualTree:
    def __init__(self):
        ROOT = Node()
        ROOT.set_decision(lambda x: "L" if x.get('Outlook') == "Sunny" else
        "M" if x.get('Outlook') == "Overcast" else
        "R")

        # Left node
        L1 = Node()
        L1.set_decision(lambda x: "L" if x.get('Humidity') == "High" else "R")
        ROOT.LEFT_NODE = L1

        L2 = Node()
        L2.set_leaf_node(NOT_PLAY)
        L1.LEFT_NODE = L2

        L3 = Node()
        L3.set_leaf_node(PLAY)
        L1.RIGHT_NODE = L3

        # Middle node
        M1 = Node()
        M1.set_leaf_node(PLAY)
        ROOT.MIDDLE_NODE = M1

        # Right Node
        R1 = Node()
        R1.set_decision(lambda x: "L" if x.get('Wind') == "Weak" else "R")
        ROOT.RIGHT_NODE = R1

        R2 = Node()
        R2.set_leaf_node(PLAY)
        R1.LEFT_NODE = R2

        R3 = Node()
        R3.set_leaf_node(NOT_PLAY)
        R3.RIGHT_NODE = R3

        self.ROOT = ROOT

    def predict_one(self, sample):
        return self.ROOT.evaluate(sample)


class Node:
    LEFT_NODE = None
    MIDDLE_NODE = None
    RIGHT_NODE = None
    leafValue = None
    isLeaf = None
    name = None
    decisionFunctions = None

    def __init__(self):
        pass

    def set_leaf_node(self, leaf_value):
        self.isLeaf = True
        self.leafValue = leaf_value

    def set_left_node(self, node):
        self.LEFT_NODE = node

    def set_middle_node(self, node):
        self.MIDDLE_NODE = node

    def set_right_node(self, node):
        self.RIGHT_NODE = node

    def set_decision(self, decision_functions):
        self.decisionFunctions = decision_functions

    def evaluate(self, sample):
        # Instance current node is leaf -> return result predict
        if self.isLeaf:
            return self.leafValue

        # Current node != leaf
        next_step = self.decisionFunctions(sample)
        if next_step == "L":
            return self.LEFT_NODE.evaluate(sample)
        elif next_step == "M":
            return self.MIDDLE_NODE.evaluate(sample)
        else:
            return self.RIGHT_NODE.evaluate(sample)
